/**
 * Treap implementation
 * Copyright (C) 2012  Jörn Magens<shuerhaaken@googlemail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// -----
// Generic randomized binary tree implementation for the vala language
// compiled with 'valac -X "-O2" treap.vala' or just 'valac treap.vala'
// -----
// some commented tests are at the bottom of this file

public class Treap<K,V> {
  private int _count;
  private Node<K,V>? root = null;
  
  [Compact]
  private class Node<K,V> {
    public K key;
    public V data;
    
    public uint priority;
    public Node<K,V>? left  = null;
    public Node<K,V>? right = null;
    
    public Node(owned K key, owned V? data) {
      this.key = key;
      this.data = data;
    }
    
    //~Node() {
    //  print("dtor node\n");
    //}
  }
  
  private CompareDataFunc key_comparefunc;
  
  public Treap() {
    key_comparefunc = get_compare_func_for(typeof(K));
  }

  public int count() {
    return this._count;
  }

  public new void set(owned K key, owned V data) {
    this.insert(key, data);
  }
  
  public new V get(K key) {
    return this.node_get(this.root, key);
  }
  
  private V node_get(Node<K,V>? node, V key) {
    if(node == null)
      return null;
    
    int c = key_comparefunc(key, node.key);
    if(c < 0)
      return this.node_get(node.left, key);
    
    if(c > 0)
      return this.node_get(node.right, key);
    
    assert(c == 0);
    return node.data;
  }

  public bool contains(K key) {
    return this.node_available(this.root, key);
  }

  private bool node_available(Node<K,V>? node, K key) {
    if(node == null)
      return false;
    
    int c = key_comparefunc(key, node.key);
    
    if(c < 0)
      return this.node_available(node.left, key);
    
    if(c > 0)
      return this.node_available(node.right, key);
    
    return true;
  }

  private static uint get_priority(ref Node<K,V> node) {
    uint key = (uint)((void*)node);
    /* This hash function is based on one found on Thomas Wang's
    * web page at http://www.concentric.net/~Ttwang/tech/inthash.htm
    */
    key = (key << 15) - key - 1;
    key = key ^(key >> 12);
    key = key + (key << 2);
    key = key ^(key >> 4);
    key = key + (key << 3) + (key << 11);
    key = key ^(key >> 16);
    
    return key != 0 ? key : 1;
  }
  
  // Insert an data into the tree.
  public void insert(owned K key, owned V data) {
    this.node_insert(ref this.root, key, data);//, priority);
  }
  
  private void node_insert(ref Node<K,V>? node, owned K key, owned V data) {//, uint priority) {
    if(node == null) {
      this._count++;
      node = new Node<K,V>(key, data);//, priority);
      node.priority = get_priority(ref node);
      return;
    }
    int c = key_comparefunc(key, node.key);
    if(c < 0) {
      this.node_insert(ref node.left, key, data); //, priority);
      if(node.left.priority < node.priority) {
        this.rotate_left(ref node);
        return;
      }
      return;
    }
    if(c > 0) {
      this.node_insert(ref node.right, key, data); //, priority);
      if(node.right.priority < node.priority) {
        this.rotate_right(ref node);
        return;
      }
      return;
    }
    // equal: replace the value
    assert(c == 0);
    node.data = data;
  }

  private void rotate_left(ref Node<K,V> node) {
    Node<K,V>? result = (owned) node.left;
    node.left = (owned) result.right;
    result.right = (owned) node;
    node = (owned)result;
  }

  private void rotate_right(ref Node<K,V> node) {
    Node<K,V>? result = (owned)node.right;
    node.right = (owned)result.left;
    result.left = (owned)node;
    node = (owned)result;
  }

  public void clear() {
    this._count = 0;
    this.root = null;
  }
  
  private void merge(ref Node<K,V>? left, ref Node<K,V>? right, out Node<K,V>? node) {
    if(left == null) {
      node = (owned)right;
      return;
    }
    if(right == null) {
      node = (owned)left;
      return;
    }
    if(left.priority < right.priority) {
      this.merge(ref left.right, ref right, out left.right);
      node = (owned)left;
      return;
    }
    this.merge(ref right.left, ref left, out right.left);
    node = (owned)right;
  }

  public void remove(K key) {
    if(!(key in this))
      return;
    
    this.node_remove(ref this.root, key);
  }

  private void node_remove(ref Node<K,V>? node, K key) {
    if(node == null)
      return;
    
    int c = key_comparefunc(key, node.key);
    if(c < 0) {
      this.node_remove(ref node.left, key);
      return;
    }
    if(c > 0) {
      this.node_remove(ref node.right, key);
      return;
    }
    this._count--;
    this.merge(ref node.left, ref node.right, out node);
  }

  public int depth(ref K key) {
    return this.node_depth(ref this.root, ref key);
  }

  private int node_depth(ref Node<K,V>? node, ref K key) {
    if(node == null)
      return 0;
    
    int c = key_comparefunc(key, node.key);
    if(c < 0) {
      int depth = this.node_depth(ref node.left, ref key);
      return depth + 1;
    }
    if(c > 0) {
      int depth = this.node_depth(ref node.right, ref key);
      return depth + 1;
    }
    return 0;
  }

  public class Iterator<K,V> {
    private Treap<K,V> _treap;
    private List<unowned Node<K,V>?> nds = null;
    
    public Iterator(Treap<K,V> treap) {
      _treap = treap;
      collect_keys(ref _treap.root);
      if(nds != null)
        nds.reverse();
    }
    
    private void collect_keys(ref Node<K,V>? h) {
      if(h == null)
        return;
      collect_keys(ref h.left);
      if(nds == null)
        nds = new List<unowned Node<K,V>?>();
      unowned Node<K,V> j = h;
      nds.prepend(j);
      collect_keys(ref h.right);
    }
    
    private uint i = 0;
    public bool next () {
      if(nds == null)
        return false;
      if(nds.nth(i) == null)
        return false;
      return true;
    }
    
    public new K get () {
      return nds.nth_data(i++).key;
    }
  }
  
  public Iterator<K,V> iterator() {
    return new Iterator<K,V> (this);
  }
  
  public V max() {
    return node_max(ref this.root);
  }

  private V node_max(ref Node<K,V>? h) {
    if(h == null)
      return null;
    
    if(h.right == null) 
      return h.data;
    
    return node_max(ref h.right);
  }

  public V min() {
    return node_min(ref this.root);
  }

  private V node_min(ref Node<K,V>? h) {
    if(h == null)
      return null;
    
    if(h.left == null)
      return h.data;
    
    return node_min(ref h.left);
  }
  
  private static CompareDataFunc get_compare_func_for(Type t) {
    if(t == typeof(string)) {
      return (a, b) => {
        if (a == b)
          return 0;
        else if (a == null)
          return -1;
        else if (b == null)
          return 1;
        else
          return strcmp((string) a, (string) b);
      };
    }
    else if(t == typeof(int)) {
      return (a, b) => {
        if((int)a < (int)b)
          return -1;
        else if ((int)a > (int)b)
          return 1;
        return 0;
      };
    }
    else if(t == typeof(int32)) {
      return (a, b) => {
        if((int32)a < (int32)b)
          return -1;
        else if ((int32)a > (int32)b)
          return 1;
        return 0;
      };
    }
    else if(t == typeof(uint)) {
      return (a, b) => {
        if((uint)a < (uint)b)
          return -1;
        else if ((uint)a > (uint)b)
          return 1;
        return 0;
      };
    }
    else if(t == typeof(uint32)) {
      return (a, b) => {
        if((uint32)a < (uint32)b)
          return -1;
        else if ((uint32)a > (uint32)b)
          return 1;
        return 0;
      };
    }
//    else if(t == typeof(double)) {
//      return (a, b) => {
//        double a_d = a, b_d = b;
//        if(a_d < b_d)
//          return -1;
//        else if (a_d > b_d)
//          return 1;
//        return 0;
//      };
//    }
//    else if(t == typeof(float)) {
//      return (a, b) => {
//        if((float)a < (float)b)
//          return -1;
//        else if ((float)a > (float)b)
//          return 1;
//        return 0;
//      };
//    }
    else {
      return (_val1, _val2) => {
        long val1 = (long)_val1, val2 = (long)_val2;
        if(val1 > val2) {
          return 1;
        }
        else if (val1 == val2) {
          return 0;
        }
        else {
          return -1;
        }
      };
    }
  }
}



void main() {
  Treap<int,string> tree = new Treap<int,string>();
  for(int i = 0; i < 100000; i++) {
    tree[i] = "val" + i.to_string();
  }
//  print("tree.count(): %d\n", tree.count());
//  if(tree.count() != 100000) {
//    print("expected count of 1000\n");
//  }
////  foreach(string s in tree)
////    print("%s:%s\n", s, tree[s]);
//  
//  tree.clear();
//  string? x = tree.get(99);
//  if(x != null) {
//    print("expected null for nonexistent key\n");
//  }
//  tree.insert(1, "1a");
//  print("inserted: %s\n", tree[1]);
//  tree.insert(2, "2a");
//  tree.insert(3, "3a");
//  tree.insert(4, "4a");
//  tree.insert(5, "5a");
//  tree.insert(6, "6a");
//  tree.insert(7, "7a");
//  string y = tree.get(6);
//  tree.remove(5);
//  if(y != "6a") {
//    print("expected 6a, got %s \n", y);
//  }
//  else {
//    print("worked as expected : %s %d\n", y, tree.count());
//  }
//  foreach(int k in tree) {
//    print("key: %d : %s\n", k, tree[k]);
//  }
//  print("clear\n");
//  tree.clear();
//  print("cleared\n");
//  Treap<int,bool> tree2 = new Treap<int,bool>();
//  for(int i = 0; i < 1000; i++) {
//    tree2.insert(i, false);
//  }
//  for(int i = 0; i < 1000; i += 50) {
//    print("%d: depth = %d\n", i, tree2.depth(ref i));
//  }
//  foreach(int k in tree2) {
//    print(" %d ", k);
//  }
}


